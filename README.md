# Onion guard

Application that monitors status of other apps.

### Key features:

* Defining apps to be monitored (ApplicationRegistry)
* Checking apps status periodically
* Checking app status history
* Sending alert notifications