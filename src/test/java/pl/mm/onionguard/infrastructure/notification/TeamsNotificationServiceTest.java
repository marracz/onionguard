package pl.mm.onionguard.infrastructure.notification;

import com.github.tomakehurst.wiremock.WireMockServer;
import okhttp3.OkHttpClient;
import org.junit.Rule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

class TeamsNotificationServiceTest {

    private static final int PORT = 8089;

    @Rule
    private static final WireMockServer SERVER = new WireMockServer(PORT);

    @BeforeEach
    void setUp() {
        SERVER.start();
    }

    @AfterEach
    void tearDown() {
        SERVER.stop();
    }

    @Test
    public void shouldSendNotification() {

        // given
        TeamsNotificationService teamsNotificationService = new TeamsNotificationService(
                new OkHttpClient().newBuilder().build(), "http://localhost:8089/notify");

        // when
        SERVER.stubFor(
                post(urlEqualTo("/notify"))
                        .withRequestBody(containing("test notification"))
                        .willReturn(
                                aResponse()
                                        .withHeader("Content-Type", "text/plain")
                                        .withBody("OK")));

        teamsNotificationService.sendNotification("test notification");

    }

}