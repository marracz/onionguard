package pl.mm.onionguard.domain.monitoring;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import pl.mm.onionguard.domain.monitoring.port.HistorySaveService;
import pl.mm.onionguard.domain.monitoring.port.NotifierService;
import pl.mm.onionguard.domain.monitoring.port.ReplicaStatusChecker;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = MonitoringConfiguration.class)
public class MonitoringConfigurationTest {

    @Autowired
    private MonitoringService monitoringService;

    @MockBean
    private ReplicaStatusChecker replicaStatusChecker;
    @MockBean
    private NotifierService notifierService;
    @MockBean
    private HistorySaveService historySaveService;

    @Test
    public void monitoringConfiguration() {
        assertThat(monitoringService).isInstanceOf(MonitoringService.class);
    }

}
