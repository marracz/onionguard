package pl.mm.onionguard.domain.monitoring;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pl.mm.onionguard.domain.monitoring.model.*;
import pl.mm.onionguard.domain.monitoring.port.HistorySaveService;
import pl.mm.onionguard.domain.monitoring.port.NotifierService;
import pl.mm.onionguard.domain.monitoring.port.ReplicaStatusChecker;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class MonitoringServiceTest {

    private MonitoringService sut;
    @Mock
    private ReplicaStatusChecker replicaStatusChecker;
    @Mock
    private NotifierService notifierService;
    @Mock
    private HistorySaveService historySaveService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        this.sut = new MonitoringService(replicaStatusChecker, notifierService, historySaveService);
    }

    @Test
    public void shouldReturnApplicationStatusOk_allReplicasOk() {
        //given
        final ReplicaCheckCommand replicaCheckCommand1 = replica("10.2.2.2");
        final ReplicaCheckCommand replicaCheckCommand2 = replica("10.2.4.1");
        ApplicationCheckCommand applicationCheckCommand = new ApplicationCheckCommand("app1", Set.of(replicaCheckCommand1, replicaCheckCommand2));

        //when
        when(replicaStatusChecker.check(replicaCheckCommand1)).thenReturn(new ReplicaStatus("10.2.2.2", Status.OK));
        when(replicaStatusChecker.check(replicaCheckCommand2)).thenReturn(new ReplicaStatus("10.2.4.1", Status.OK));
        final ApplicationStatus applicationStatus = sut.checkStatus(applicationCheckCommand);

        //then
        assertThat(applicationStatus.getStatus()).isEqualTo(Status.OK);
        verifyNoInteractions(notifierService);
        verify(historySaveService).save(any(LocalDateTime.class), any(ApplicationStatus.class));
    }

    @Test
    public void shouldReturnApplicationStatusFail_oneOfReplicasFail() {
        //given
        final ReplicaCheckCommand replicaCheckCommand1 = replica("10.2.2.2");
        final ReplicaCheckCommand replicaCheckCommand2 = replica("10.2.4.1");
        ApplicationCheckCommand applicationCheckCommand = new ApplicationCheckCommand("app2", Set.of(replicaCheckCommand1, replicaCheckCommand2));

        //when
        when(replicaStatusChecker.check(replicaCheckCommand1)).thenReturn(new ReplicaStatus("10.2.2.2", Status.OK));
        when(replicaStatusChecker.check(replicaCheckCommand2)).thenReturn(new ReplicaStatus("10.2.4.1", Status.FAIL));
        final ApplicationStatus applicationStatus = sut.checkStatus(applicationCheckCommand);

        //then
        assertThat(applicationStatus.getStatus()).isEqualTo(Status.FAIL);
        verify(notifierService).notifyFailure(eq(applicationCheckCommand), any(Collection.class));
        verify(historySaveService).save(any(LocalDateTime.class), any(ApplicationStatus.class));
    }

    private ReplicaCheckCommand replica(String ip) {
        return new ReplicaCheckCommand(ip, "/health");
    }
}