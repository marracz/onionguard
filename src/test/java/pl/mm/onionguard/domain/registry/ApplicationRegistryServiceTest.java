package pl.mm.onionguard.domain.registry;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pl.mm.onionguard.domain.registry.model.Application;
import pl.mm.onionguard.domain.registry.model.ApplicationFetchFailedException;
import pl.mm.onionguard.domain.registry.port.ApplicationRepository;

import java.util.Collection;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class ApplicationRegistryServiceTest {

    private ApplicationRegistryService sut;

    @Mock
    private ApplicationRepository applicationRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        sut = new ApplicationRegistryService(applicationRepository);
    }

    @Test
    public void shouldReturnDefinedApps() {
        //given
        final Application app1 = application("app1");
        final Application app2 = application("app2");

        //when
        when(applicationRepository.findAll()).thenReturn(List.of(app1, app2));
        final Collection<Application> response = sut.findAll();

        //then
        assertThat(response).containsExactlyInAnyOrder(app1, app2);
    }

    @Test
    public void shouldReturnEmptyList() {

        //when
        when(applicationRepository.findAll()).thenThrow(ApplicationFetchFailedException.class);
        final Collection<Application> response = sut.findAll();

        //then
        assertThat(response).isEmpty();;
    }

    private Application application(String appName) {
        return Application.builder().name(appName).build();
    }
}