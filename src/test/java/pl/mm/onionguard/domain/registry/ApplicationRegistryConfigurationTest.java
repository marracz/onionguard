package pl.mm.onionguard.domain.registry;

import org.junit.jupiter.api.Test;
import pl.mm.onionguard.domain.registry.port.ApplicationRepository;

import static org.mockito.Mockito.mock;

public class ApplicationRegistryConfigurationTest {

    @Test
    public void createK8sApplicationRepository() {
        ApplicationRegistryConfiguration configuration = new ApplicationRegistryConfiguration();
        configuration.applicationRegistryService(mock(ApplicationRepository.class));
    }
}
