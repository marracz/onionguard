package pl.mm.onionguard.infrastructure.registry;

import io.kubernetes.client.openapi.ApiClient;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import pl.mm.onionguard.domain.registry.port.ApplicationRepository;

@TestConfiguration
public class K8sApplicationRepositoryTestConfiguration {

    private static final int PORT = 8089;

    @Bean
    public ApplicationRepository applicationRepository() {
        return new K8sApplicationRepository(apiClient());
    }

    private ApiClient apiClient() {
        ApiClient client = new ApiClient();
        client.setBasePath("http://localhost:" + PORT);
        return client;
    }
}
