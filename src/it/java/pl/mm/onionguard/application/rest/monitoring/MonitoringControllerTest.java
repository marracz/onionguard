package pl.mm.onionguard.application.rest.monitoring;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.openapi.models.*;
import org.junit.Rule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import pl.mm.onionguard.domain.monitoring.model.ApplicationStatus;
import pl.mm.onionguard.domain.monitoring.model.Status;
import pl.mm.onionguard.infrastructure.registry.K8sApplicationRepositoryTestConfiguration;

import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@AutoConfigureWebMvc
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@Import(K8sApplicationRepositoryTestConfiguration.class)
@ActiveProfiles("test")
public class MonitoringControllerTest {

    private static final int PORT = 8089;

    @Rule
    private static final WireMockServer SERVER = new WireMockServer(PORT);

    @BeforeEach
    void setUp() {
        SERVER.start();
    }

    @AfterEach
    void tearDown() {
        SERVER.stop();
    }

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturnApplicationStatusOk() throws Exception {

        // given
        ApiClient client = new ApiClient();

        V1Pod pod1 = new V1Pod()
                .metadata(new V1ObjectMeta().name("app1-replica1").ownerReferences(List.of(new V1OwnerReference().name("app1"))))
                .spec(new V1PodSpec().containers(List.of(new V1Container().name("A").readinessProbe(new V1Probe().httpGet(new V1HTTPGetAction().path("/health"))))))
                .status(new V1PodStatus().podIP("localhost:8089").phase("Running"));
        V1Pod pod2 = new V1Pod()
                .metadata(new V1ObjectMeta().name("app1-replica2").ownerReferences(List.of(new V1OwnerReference().name("app1"))))
                .spec(new V1PodSpec().containers(List.of(new V1Container().name("A").readinessProbe(new V1Probe().httpGet(new V1HTTPGetAction().path("/health"))))))
                .status(new V1PodStatus().podIP("localhost:8089").phase("Running"));

        SERVER.stubFor(
                get(urlEqualTo("/api/v1/namespaces/payment-systems/pods"))
                        .willReturn(
                                aResponse()
                                        .withHeader("Content-Type", "application/json")
                                        .withBody(client.getJSON().serialize(new V1PodList().items(List.of(pod1, pod2))))));

        SERVER.stubFor(
                get(urlEqualTo("/health"))
                        .willReturn(
                                aResponse()
                                        .withHeader("Content-Type", "text/plain")
                                        .withBody("OK")));

        // when
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/runtest/app1"))
                .andExpect(status().isOk())
                .andReturn();

        // then
        ApplicationStatus response = MAPPER.readValue(mvcResult.getResponse().getContentAsString(), ApplicationStatus.class);
        assertThat(response.getStatus()).isEqualTo(Status.OK);
    }

    @Test
    public void shouldReturnApplicationStatusFail() throws Exception {

        // given
        ApiClient client = new ApiClient();

        V1Pod pod1 = new V1Pod()
                .metadata(new V1ObjectMeta().name("app1-replica1").ownerReferences(List.of(new V1OwnerReference().name("app1"))))
                .spec(new V1PodSpec().containers(List.of(new V1Container().name("A").readinessProbe(new V1Probe().httpGet(new V1HTTPGetAction().path("/health"))))))
                .status(new V1PodStatus().podIP("localhost:8089").phase("Running"));
        V1Pod pod2 = new V1Pod()
                .metadata(new V1ObjectMeta().name("app1-replica2").ownerReferences(List.of(new V1OwnerReference().name("app1"))))
                .spec(new V1PodSpec().containers(List.of(new V1Container().name("A").readinessProbe(new V1Probe().httpGet(new V1HTTPGetAction().path("/health"))))))
                .status(new V1PodStatus().podIP("localhost:8111").phase("Running"));
        V1Pod pod3 = new V1Pod()
                .metadata(new V1ObjectMeta().name("app1-replica3").ownerReferences(List.of(new V1OwnerReference().name("app1"))))
                .spec(new V1PodSpec().containers(List.of(new V1Container().name("A").readinessProbe(new V1Probe().httpGet(new V1HTTPGetAction().path("/fail"))))))
                .status(new V1PodStatus().podIP("localhost:8089").phase("Running"));

        SERVER.stubFor(
                get(urlEqualTo("/api/v1/namespaces/payment-systems/pods"))
                        .willReturn(
                                aResponse()
                                        .withHeader("Content-Type", "application/json")
                                        .withBody(client.getJSON().serialize(new V1PodList().items(List.of(pod1, pod2, pod3))))));

        SERVER.stubFor(
                get(urlEqualTo("/health"))
                        .willReturn(
                                aResponse()
                                        .withHeader("Content-Type", "text/plain")
                                        .withBody("OK")));

        SERVER.stubFor(
                get(urlEqualTo("/fails"))
                        .willReturn(
                                aResponse()
                                        .withStatus(404)));

        // when
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/runtest/app1"))
                .andExpect(status().isOk())
                .andReturn();

        // then
        ApplicationStatus response = MAPPER.readValue(mvcResult.getResponse().getContentAsString(), ApplicationStatus.class);
        assertThat(response.getStatus()).isEqualTo(Status.FAIL);
    }

    @Test
    public void shouldThrowApplicationNotFoundException() throws Exception {

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/runtest/app2"))
                .andExpect(status().isNotFound())
                .andReturn();

        assertThat(mvcResult.getResponse().getContentAsString()).isEqualTo("Application app2 not exists");

    }

}
