package pl.mm.onionguard.application.rest.registry;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.openapi.models.*;
import org.junit.Rule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import pl.mm.onionguard.domain.registry.ApplicationRegistryTestConfiguration;
import pl.mm.onionguard.infrastructure.registry.K8sApplicationRepositoryTestConfiguration;

import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@AutoConfigureWebMvc
@SpringBootTest(classes = {
        ApplicationRegistryController.class,
        ApplicationRegistryRestService.class,
        ApplicationRegistryTestConfiguration.class,
        K8sApplicationRepositoryTestConfiguration.class
 })
class ApplicationRegistryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private static final int PORT = 8089;
    @Rule
    private static final WireMockServer SERVER = new WireMockServer(PORT);

    @BeforeEach
    void setUp() {
        SERVER.start();
    }

    @AfterEach
    void tearDown() {
        SERVER.stop();
    }

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Test
    public void shouldReturnAllApplications() throws Exception {
        // given
        ApiClient client = new ApiClient();

        V1Pod pod1 = new V1Pod()
                .metadata(new V1ObjectMeta().name("app1-replica1").ownerReferences(List.of(new V1OwnerReference().name("app1"))))
                .spec(new V1PodSpec().containers(List.of(new V1Container().name("A").readinessProbe(new V1Probe().httpGet(new V1HTTPGetAction().path("/health"))))))
                .status(new V1PodStatus().podIP("10.2.2.2").phase("Running"));
        V1Pod pod2 = new V1Pod()
                .metadata(new V1ObjectMeta().name("app1-replica2").ownerReferences(List.of(new V1OwnerReference().name("app1"))))
                .spec(new V1PodSpec().containers(List.of(new V1Container().name("A").readinessProbe(new V1Probe().httpGet(new V1HTTPGetAction().path("/health"))))))
                .status(new V1PodStatus().podIP("10.2.2.3").phase("Running"));
        V1Pod pod3 = new V1Pod();
        V1Pod pod4 = new V1Pod().metadata(new V1ObjectMeta().name("app1-replica3"));

        SERVER.stubFor(
                get(urlEqualTo("/api/v1/namespaces/payment-systems/pods"))
                        .willReturn(
                                aResponse()
                                        .withHeader("Content-Type", "application/json")
                                        .withBody(client.getJSON().serialize(new V1PodList().items(List.of(pod1, pod2, pod3, pod4))))));

        // when
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/applications"))
                .andExpect(status().isOk())
                .andReturn();

        // then
        List<ApplicationResponse> applications = MAPPER.readValue(mvcResult.getResponse().getContentAsString(),
                new TypeReference<>() {});
        assertThat(applications).hasSize(1);
        assertThat(applications.get(0).getName()).isEqualTo("app1");
        assertThat(applications.get(0).getReplicas()).containsExactlyInAnyOrder(
                new ReplicaResponse("10.2.2.2", "/health"), new ReplicaResponse("10.2.2.3", "/health"));
    }

    @Test
    public void shouldReturnEmptyCollection() throws Exception {
        // given
        ApiClient client = new ApiClient();

        SERVER.stubFor(
                get(urlEqualTo("/api/v1/namespaces/payment-systems/pods"))
                        .willReturn(
                                aResponse()
                                        .withHeader("Content-Type", "application/json")
                                        .withStatus(500)));

        // when
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/applications"))
                .andExpect(status().isOk())
                .andReturn();

        // then
        List<ApplicationResponse> applications = MAPPER.readValue(mvcResult.getResponse().getContentAsString(),
                new TypeReference<>() {});
        assertThat(applications).isEmpty();
    }

}