package pl.mm.onionguard.application.rest.history;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import pl.mm.onionguard.domain.history.HistoryServiceTestConfiguration;
import pl.mm.onionguard.domain.history.model.ApplicationStatusSaveCommand;
import pl.mm.onionguard.domain.history.model.Status;
import pl.mm.onionguard.domain.history.port.HistoryRegistry;
import pl.mm.onionguard.infrastructure.history.HistoryRegistryTestConfiguration;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@AutoConfigureWebMvc
@SpringBootTest(classes = {
        HistoryController.class,
        HistoryRestService.class,
        HistoryServiceTestConfiguration.class,
        HistoryRegistryTestConfiguration.class
})
@EnableJpaRepositories(basePackages = "pl.mm.onionguard.infrastructure.history")
@EntityScan(basePackages = "pl.mm.onionguard.infrastructure.history")
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@AutoConfigureTestEntityManager
@AutoConfigureDataJpa
public class HistoryControllerTest {

    @Autowired
    private HistoryRegistry historyRegistry;

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturnApplicationStatusOk() throws Exception {

        //given
        historyRegistry.save(new ApplicationStatusSaveCommand(LocalDateTime.now(), Status.OK, "app1"));
        historyRegistry.save(new ApplicationStatusSaveCommand(LocalDateTime.now(), Status.FAIL, "app1"));

        // when
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/history/app1"))
                .andExpect(status().isOk())
                .andReturn();

        //then
        List<ApplicationHistoryResponse> response = MAPPER.readValue(mvcResult.getResponse().getContentAsString(),
                new TypeReference<>() {});
        assertThat(response).hasSize(2);
        assertThat(response.get(0).getName()).isEqualTo("app1");
        assertThat(response.get(0).getStatus()).isEqualTo("OK");
        assertThat(response.get(1).getName()).isEqualTo("app1");
        assertThat(response.get(1).getStatus()).isEqualTo("FAIL");
    }
}
