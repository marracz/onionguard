package pl.mm.onionguard.domain.registry;

import org.springframework.boot.test.context.TestConfiguration;

@TestConfiguration
public class ApplicationRegistryTestConfiguration extends ApplicationRegistryConfiguration {
}
