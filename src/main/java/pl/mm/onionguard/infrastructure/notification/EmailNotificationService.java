package pl.mm.onionguard.infrastructure.notification;

import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import pl.mm.onionguard.domain.notification.port.NotificationChannel;

@Slf4j
class EmailNotificationService implements NotificationChannel {

    private final JavaMailSender emailSender;
    private final String sender;
    private final String receiver;

    private static final String SUBJECT = "Application status failed";

    public EmailNotificationService(JavaMailSender emailSender,
                                    String sender,
                                    String receiver) {
        this.emailSender = emailSender;
        this.sender = sender;
        this.receiver = receiver;
    }

    @Override
    public void sendNotification(String msg) {
        log.warn("channel [email] {}", msg);

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(this.sender);
        message.setTo(this.receiver);
        message.setSubject(SUBJECT);
        message.setText(msg);
        emailSender.send(message);
    }
}
