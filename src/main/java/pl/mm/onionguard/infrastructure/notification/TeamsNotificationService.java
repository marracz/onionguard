package pl.mm.onionguard.infrastructure.notification;

import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.springframework.http.HttpHeaders;
import org.springframework.util.ResourceUtils;
import pl.mm.onionguard.domain.notification.port.NotificationChannel;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
class TeamsNotificationService implements NotificationChannel {

    private final OkHttpClient httpClient;
    private final String webhookUrl;

    public TeamsNotificationService(OkHttpClient httpClient, String webhookUrl) {
        this.httpClient = httpClient;
        this.webhookUrl = webhookUrl;
    }

    @Override
    public void sendNotification(String msg) {
        log.warn("channel [teams] {}", msg);
        File file;
        try {
            file = ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX + "teams-notification-template.json");
            String template = new String(Files.readAllBytes(file.toPath()));

            RequestBody formBody = RequestBody.create(String.format(template, msg, ""),
                    MediaType.parse("application/json")
            );
            Request request = new Request.Builder()
                    .url(webhookUrl)
                    .addHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON_VALUE)
                    .post(formBody)
                    .build();
            httpClient.newCall(request).execute();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }


    }
}
