package pl.mm.onionguard.infrastructure.notification;

import lombok.extern.slf4j.Slf4j;
import pl.mm.onionguard.domain.notification.port.NotificationChannel;

@Slf4j
class LogNotificationService implements NotificationChannel {
    @Override
    public void sendNotification(String msg) {
        log.warn("channel [log] {}", msg);
    }
}
