package pl.mm.onionguard.infrastructure.notification;

import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import pl.mm.onionguard.domain.notification.port.NotificationChannel;

import java.util.concurrent.TimeUnit;

@Configuration
class NotificationsAdaptersConfiguration {

    @Bean
    @ConditionalOnProperty(name="mail-notification", havingValue = "true")
    NotificationChannel emailNotificationChannel(
            JavaMailSender emailSender,
            @Value("@{}") String sender,
            @Value("@{}") String receiver) {
        return new EmailNotificationService(emailSender, sender, receiver);
    }

    @Bean
    @ConditionalOnProperty(name="log-notification", havingValue = "true")
    NotificationChannel logNotificationChannel() {
        return new LogNotificationService();
    }

    @Bean
    @ConditionalOnProperty(name="teams-notification", havingValue = "true")
    NotificationChannel teamsNotificationChannel(
            @Value("${teams.webhook.url}") String webhookUrl) {
        return new TeamsNotificationService(httpClient(), webhookUrl);
    }

    private OkHttpClient httpClient() {
        return new OkHttpClient().newBuilder()
                .connectTimeout(1000, TimeUnit.MILLISECONDS)
                .readTimeout(1000, TimeUnit.MILLISECONDS)
                .build();
    }
}
