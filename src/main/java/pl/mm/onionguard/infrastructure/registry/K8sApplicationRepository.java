package pl.mm.onionguard.infrastructure.registry;

import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.apis.CoreV1Api;
import io.kubernetes.client.openapi.models.*;
import pl.mm.onionguard.domain.registry.model.Application;
import pl.mm.onionguard.domain.registry.model.Replica;
import pl.mm.onionguard.domain.registry.model.ApplicationFetchFailedException;
import pl.mm.onionguard.domain.registry.port.ApplicationRepository;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

class K8sApplicationRepository implements ApplicationRepository {

    private final ApiClient client;

    public K8sApplicationRepository(ApiClient client) {
        this.client = client;
    }

    @Override
    public Collection<Application> findAll() throws ApplicationFetchFailedException {
        CoreV1Api api = new CoreV1Api(client);
        try {
            V1PodList pods =
                    api.listNamespacedPod("payment-systems", null,
                            null, null, null, null,
                            null, null, null, null);
            Map<V1OwnerReference, List<V1Pod>> podsWithOwner = pods.getItems().stream()
                    .filter(pod -> pod.getMetadata() != null)
                    .filter(pod -> pod.getMetadata().getOwnerReferences() != null)
                    .filter(pod -> "Running".equals(pod.getStatus().getPhase()))
                    .collect(Collectors.groupingBy(pod -> pod.getMetadata().getOwnerReferences().get(0)));
            return podsWithOwner.entrySet().stream()
                    .map(entry -> Application.builder()
                            .name(entry.getKey().getName())
                            .replicas(entry.getValue().stream().map(pod -> Replica.builder()
                                    .ip(pod.getStatus().getPodIP())
                                    .healthcheck(getHealthcheck(pod)).build()).collect(Collectors.toSet()))
                            .build())
                    .collect(Collectors.toSet());
        } catch (ApiException e) {
            throw new ApplicationFetchFailedException(e.getMessage(), e);
        }
    }

    private String getHealthcheck(V1Pod pod) {
        return pod.getSpec().getContainers().stream()
                .filter(Objects::nonNull)
                .map(V1Container::getReadinessProbe)
                .filter(Objects::nonNull)
                .map(V1Probe::getHttpGet)
                .filter(Objects::nonNull)
                .map(V1HTTPGetAction::getPath)
                .findFirst().orElse("/");
    }
}
