package pl.mm.onionguard.infrastructure.registry;

import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.util.ClientBuilder;
import io.kubernetes.client.util.KubeConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import pl.mm.onionguard.domain.registry.port.ApplicationRepository;

import java.io.FileReader;
import java.io.IOException;

@Profile("!test")
@Configuration
class K8sApplicationRepositoryConfiguration {

    private static final String KUBE_CONFIG_PATH = System.getenv("HOME") + "/.kube/config";

    @Bean
    public ApplicationRepository applicationRepository() throws IOException {
        return new K8sApplicationRepository(apiClient());
    }

    private ApiClient apiClient() throws IOException {

        return ClientBuilder.kubeconfig(KubeConfig.
                loadKubeConfig(new FileReader(KUBE_CONFIG_PATH))).build();
    }
}
