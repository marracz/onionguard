package pl.mm.onionguard.infrastructure.history;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.mm.onionguard.domain.history.port.HistoryRegistry;

@Configuration
class DatabaseHistoryRepositoryAdapterConfiguration {

    @Bean
    public HistoryRegistry databaseRepository(JdbcTestRepository jdbcTestRepository) {
        return new DatabaseRepository(jdbcTestRepository);
    }
}
