package pl.mm.onionguard.infrastructure.history;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
interface JdbcTestRepository extends CrudRepository<TestEntity, UUID> {

    Iterable<TestEntity> findByApplicationName(String appName);
}
