package pl.mm.onionguard.infrastructure.history;

import pl.mm.onionguard.domain.history.model.ApplicationStatusResponse;
import pl.mm.onionguard.domain.history.model.ApplicationStatusSaveCommand;
import pl.mm.onionguard.domain.history.port.HistoryRegistry;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

class DatabaseRepository implements HistoryRegistry {

    private final JdbcTestRepository jdbcTestRepository;

    DatabaseRepository(JdbcTestRepository jdbcTestRepository) {
        this.jdbcTestRepository = jdbcTestRepository;
    }


    @Transactional
    @Override
    public List<ApplicationStatusResponse> getHistory(String applicationName) {
        return StreamSupport.stream(jdbcTestRepository.findByApplicationName(applicationName).spliterator(), false)
            .map(testEntity -> new ApplicationStatusResponse(testEntity.getDateTime(), testEntity.getStatus()))
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public ApplicationStatusResponse save(ApplicationStatusSaveCommand command) {
        TestEntity testEntity = TestEntity.builder()
                .id(UUID.randomUUID())
                .applicationName(command.getApplicationName())
                .dateTime(command.getDateTime())
                .status(command.getStatus())
                .build();
        jdbcTestRepository.save(testEntity);
        return new ApplicationStatusResponse(command.getDateTime(), command.getStatus());
    }
}
