package pl.mm.onionguard.infrastructure.history;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.mm.onionguard.domain.history.model.Status;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TestEntity {

    @Id
    private UUID id;
    private LocalDateTime dateTime;
    @Enumerated(value = EnumType.STRING)
    private Status status;
    private String applicationName;
}
