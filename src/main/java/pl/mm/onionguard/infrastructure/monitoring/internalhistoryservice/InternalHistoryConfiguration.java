package pl.mm.onionguard.infrastructure.monitoring.internalhistoryservice;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.mm.onionguard.domain.history.History;
import pl.mm.onionguard.domain.monitoring.port.HistorySaveService;

@Configuration
public class InternalHistoryConfiguration {



    @Bean
    public HistorySaveService historySaveService(History historyService) {
        return new InternalHistorySaveService(historyService);
    }
}
