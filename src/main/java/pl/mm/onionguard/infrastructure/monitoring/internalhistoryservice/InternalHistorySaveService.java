package pl.mm.onionguard.infrastructure.monitoring.internalhistoryservice;

import pl.mm.onionguard.domain.history.History;
import pl.mm.onionguard.domain.history.model.ApplicationStatusSaveCommand;
import pl.mm.onionguard.domain.history.model.Status;
import pl.mm.onionguard.domain.monitoring.model.ApplicationStatus;
import pl.mm.onionguard.domain.monitoring.port.HistorySaveService;

import java.time.LocalDateTime;

class InternalHistorySaveService implements HistorySaveService {

    private final History historyService;

    public InternalHistorySaveService(History historyService) {
        this.historyService = historyService;
    }

    @Override
    public void save(LocalDateTime dateTime, ApplicationStatus applicationStatus) {
        final ApplicationStatusSaveCommand applicationStatusSaveCommand =
                new ApplicationStatusSaveCommand(dateTime, map(applicationStatus.getStatus()), applicationStatus.getApplicationName());
        historyService.save(applicationStatusSaveCommand);
    }

    private Status map(pl.mm.onionguard.domain.monitoring.model.Status status) {
        switch (status) {
            case OK:
                return Status.OK;
            case FAIL:
                return Status.FAIL;
        }
        throw new IllegalArgumentException();
    }
}
