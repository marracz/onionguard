package pl.mm.onionguard.infrastructure.monitoring.httpchecker;

import okhttp3.OkHttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.mm.onionguard.domain.monitoring.port.ReplicaStatusChecker;

import java.util.concurrent.TimeUnit;

@Configuration
class HttpCheckerConfiguration {

    @Bean
    public ReplicaStatusChecker replicaStatusChecker() {
        return new HttpReplicaStatusChecker(okHttpClient());
    }

    private OkHttpClient okHttpClient() {
        return new OkHttpClient().newBuilder()
                .connectTimeout(1000, TimeUnit.MILLISECONDS)
                .readTimeout(1000, TimeUnit.MILLISECONDS)
                .build();
    }
}
