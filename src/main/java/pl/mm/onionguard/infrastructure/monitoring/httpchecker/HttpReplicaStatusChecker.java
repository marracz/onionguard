package pl.mm.onionguard.infrastructure.monitoring.httpchecker;

import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.http.HttpHeaders;
import pl.mm.onionguard.domain.monitoring.model.ReplicaCheckCommand;
import pl.mm.onionguard.domain.monitoring.model.ReplicaStatus;
import pl.mm.onionguard.domain.monitoring.model.Status;
import pl.mm.onionguard.domain.monitoring.port.ReplicaStatusChecker;

import java.io.IOException;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
class HttpReplicaStatusChecker implements ReplicaStatusChecker {

    private final OkHttpClient httpClient;

    public HttpReplicaStatusChecker(OkHttpClient httpClient) {
        this.httpClient = httpClient;
    }

    @Override
    public ReplicaStatus check(ReplicaCheckCommand replicaCheckCommand) {
        log.info("Check status for replica {}", replicaCheckCommand.getIp());
        Request request = new Request.Builder()
                .url("http://".concat(replicaCheckCommand.getIp()).concat(replicaCheckCommand.getHealthcheck()))
                .addHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON_VALUE)
                .get()
                .build();
        try {
            Response response = httpClient.newCall(request).execute();
            if (response.isSuccessful()) {
                return new ReplicaStatus(replicaCheckCommand.getIp(), Status.OK);
            }
            return new ReplicaStatus(replicaCheckCommand.getIp(), Status.FAIL);
        } catch (IOException e) {
            return new ReplicaStatus(replicaCheckCommand.getIp(), Status.FAIL);
        }
    }
}
