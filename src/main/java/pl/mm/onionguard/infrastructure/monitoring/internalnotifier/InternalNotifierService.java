package pl.mm.onionguard.infrastructure.monitoring.internalnotifier;

import pl.mm.onionguard.domain.monitoring.model.ApplicationCheckCommand;
import pl.mm.onionguard.domain.monitoring.model.ReplicaStatus;
import pl.mm.onionguard.domain.monitoring.model.Status;
import pl.mm.onionguard.domain.monitoring.port.NotifierService;
import pl.mm.onionguard.domain.notification.Notifier;

import java.util.Collection;

class InternalNotifierService implements NotifierService {

    private final Notifier notificationService;

    public InternalNotifierService(Notifier notificationService) {
        this.notificationService = notificationService;
    }

    @Override
    public void notifyFailure(ApplicationCheckCommand applicationCheckCommand, Collection<ReplicaStatus> replicaStatuses) {
        replicaStatuses.stream().filter(replicaStatus -> replicaStatus.getStatus() == Status.FAIL).forEach(rs -> {
                notificationService.sendNotification("Test failed for app " + applicationCheckCommand.getName() + " in replica " + rs.getReplicaIp());
        });
    }
}
