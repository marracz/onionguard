package pl.mm.onionguard.infrastructure.monitoring.internalnotifier;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.mm.onionguard.domain.monitoring.port.NotifierService;
import pl.mm.onionguard.domain.notification.Notifier;

@Configuration
class InternalNotifierConfiguration {

    @Bean
    public NotifierService notifierService(Notifier notificationService) {
        return new InternalNotifierService(notificationService);
    }
}
