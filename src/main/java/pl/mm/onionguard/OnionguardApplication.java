package pl.mm.onionguard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnionguardApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnionguardApplication.class, args);
	}

}
