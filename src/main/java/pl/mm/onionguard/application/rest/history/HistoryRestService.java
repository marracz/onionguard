package pl.mm.onionguard.application.rest.history;

import org.springframework.stereotype.Service;
import pl.mm.onionguard.domain.history.History;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
class HistoryRestService {

    private final History history;

    public HistoryRestService(History history) {
        this.history = history;
    }

    public Collection<ApplicationHistoryResponse> fetchHistoryForApplication(String appName) {
        //not found exception
        return history.getHistory(appName).stream()
                .map(appStatus -> new ApplicationHistoryResponse(
                        appName,
                        appStatus.getDateTime().toString(),
                        appStatus.getStatus().name())
                )
                .collect(Collectors.toList());
    }
}
