package pl.mm.onionguard.application.rest.monitoring;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.mm.onionguard.domain.monitoring.model.ApplicationCheckCommand;
import pl.mm.onionguard.domain.monitoring.model.ReplicaCheckCommand;
import pl.mm.onionguard.domain.registry.model.Application;

import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
final class ApplicationMapper {

    static ApplicationCheckCommand applicationCheckCommand(Application application) {
        return new ApplicationCheckCommand(application.getName(),
                application.getReplicas().stream()
                        .map(r -> new ReplicaCheckCommand(r.getIp(), r.getHealthcheck()))
                        .collect(Collectors.toSet())
        );
    }
}
