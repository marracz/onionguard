package pl.mm.onionguard.application.rest.monitoring;

import org.springframework.stereotype.Service;
import pl.mm.onionguard.application.rest.ApplicationNotFoundException;
import pl.mm.onionguard.domain.monitoring.Monitoring;
import pl.mm.onionguard.domain.monitoring.model.ApplicationStatus;
import pl.mm.onionguard.domain.registry.ApplicationRegistry;
import pl.mm.onionguard.domain.registry.model.Application;

import java.util.Optional;

@Service
class MonitoringService {

    private final Monitoring monitoring;
    private final ApplicationRegistry applicationRegistry;


    public MonitoringService(Monitoring monitoring, ApplicationRegistry applicationRegistry) {
        this.monitoring = monitoring;
        this.applicationRegistry = applicationRegistry;
    }

    public ApplicationStatus checkApplicationStatus(String appName) {
        Optional<Application> appData = applicationRegistry.findAll().stream()
                .filter(application -> application.getName().equals(appName))
                .findFirst();
        return appData
                .map(app -> monitoring.checkStatus(ApplicationMapper.applicationCheckCommand(app)))
                .orElseThrow(()-> new ApplicationNotFoundException("Application ".concat(appName).concat(" not exists")));
    }
}
