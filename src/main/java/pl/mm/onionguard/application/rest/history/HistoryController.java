package pl.mm.onionguard.application.rest.history;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
class HistoryController {

    private final HistoryRestService historyRestService;

    @GetMapping(path = "/history/{appName}", produces = APPLICATION_JSON_VALUE)
    public Collection<ApplicationHistoryResponse> fetchHistory(@PathVariable("appName") String appName) {
        return historyRestService.fetchHistoryForApplication(appName);
    }
}
