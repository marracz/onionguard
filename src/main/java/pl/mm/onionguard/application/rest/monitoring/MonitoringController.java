package pl.mm.onionguard.application.rest.monitoring;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.mm.onionguard.domain.monitoring.model.ApplicationStatus;

@RestController
@ConditionalOnProperty(name="mode", havingValue = "web")
@RequiredArgsConstructor
class MonitoringController {

    private final MonitoringService monitoringService;

    @GetMapping("/runtest/{appName}")
    public ApplicationStatus checkApplicationStatus(@PathVariable("appName") String appName) {
        return monitoringService.checkApplicationStatus(appName);
    }
}
