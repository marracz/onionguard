package pl.mm.onionguard.application.rest.registry;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
class ReplicaResponse {

    private final String ip;
    private final String healthcheck;
}
