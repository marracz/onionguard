package pl.mm.onionguard.application.rest.registry;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.Collection;

@RequiredArgsConstructor
@Data
class ApplicationResponse {

    private final String name;
    private final Collection<ReplicaResponse> replicas;
}
