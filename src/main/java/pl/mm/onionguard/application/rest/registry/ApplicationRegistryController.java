package pl.mm.onionguard.application.rest.registry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
class ApplicationRegistryController {

    private final ApplicationRegistryRestService registryService;

    @Autowired
    public ApplicationRegistryController(ApplicationRegistryRestService registryService) {
        this.registryService = registryService;
    }

    @GetMapping(path = "/applications", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<ApplicationResponse>> listApplications() {
        return ResponseEntity.ok(registryService.findAll());
    }

}
