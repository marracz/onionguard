package pl.mm.onionguard.application.rest.history;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import pl.mm.onionguard.domain.history.model.Status;

@RequiredArgsConstructor
@Data
public class ApplicationHistoryResponse {

    private final String name;
    private final String dateTime;
    private final String status;
}
