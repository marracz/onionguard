package pl.mm.onionguard.application.rest.registry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mm.onionguard.domain.registry.ApplicationRegistry;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
class ApplicationRegistryRestService {

    private final ApplicationRegistry applicationRegistry;

    @Autowired
    public ApplicationRegistryRestService(ApplicationRegistry applicationRegistry) {
        this.applicationRegistry = applicationRegistry;
    }

    public Collection<ApplicationResponse> findAll() {
        return applicationRegistry.findAll().stream()
                .map(app -> new ApplicationResponse(app.getName(),
                        app.getReplicas().stream()
                                .map(replica -> new ReplicaResponse(replica.getIp(), replica.getHealthcheck()))
                                .collect(Collectors.toSet()))
                )
                .collect(Collectors.toSet());
    }

}
