package pl.mm.onionguard.application.scheduler;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.mm.onionguard.domain.monitoring.Monitoring;
import pl.mm.onionguard.domain.monitoring.model.ApplicationCheckCommand;
import pl.mm.onionguard.domain.monitoring.model.ReplicaCheckCommand;
import pl.mm.onionguard.domain.registry.ApplicationRegistry;

import java.util.stream.Collectors;

@EnableScheduling
@ConditionalOnProperty(name = "scheduler", havingValue = "true")
@Component
class MonitoringScheduler {

    private final Monitoring monitoring;
    private final ApplicationRegistry applicationRegistry;

    public MonitoringScheduler(Monitoring monitoring,
                               ApplicationRegistry applicationRegistry) {
        this.monitoring = monitoring;
        this.applicationRegistry = applicationRegistry;
    }

    @Scheduled(fixedDelay = 5000L)
    public void run() {
        applicationRegistry.findAll().forEach(app -> {
            ApplicationCheckCommand applicationCheckCommand = new ApplicationCheckCommand(app.getName(),
                    app.getReplicas().stream()
                            .map(r -> new ReplicaCheckCommand(r.getIp(), r.getHealthcheck()))
                            .collect(Collectors.toSet())
            );
            monitoring.checkStatus(applicationCheckCommand);
        });
    }
}
