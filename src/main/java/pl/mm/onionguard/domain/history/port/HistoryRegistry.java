package pl.mm.onionguard.domain.history.port;

import pl.mm.onionguard.domain.history.model.ApplicationStatusResponse;
import pl.mm.onionguard.domain.history.model.ApplicationStatusSaveCommand;

import java.util.List;

public interface HistoryRegistry {

    List<ApplicationStatusResponse> getHistory(String applicationName);

    ApplicationStatusResponse save(ApplicationStatusSaveCommand command);
}
