package pl.mm.onionguard.domain.history;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.mm.onionguard.domain.history.port.HistoryRegistry;

@Configuration
class HistoryConfiguration {

    @Bean
    public History historyService(HistoryRegistry historyRegistry) {
        return new HistoryService(historyRegistry);
    }
}
