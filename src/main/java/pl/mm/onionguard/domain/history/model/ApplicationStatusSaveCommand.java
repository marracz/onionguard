package pl.mm.onionguard.domain.history.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;

@RequiredArgsConstructor
@Data
public class ApplicationStatusSaveCommand {

    private final LocalDateTime dateTime;
    private final Status status;
    private final String applicationName;
}
