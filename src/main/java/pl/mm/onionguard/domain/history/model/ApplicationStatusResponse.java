package pl.mm.onionguard.domain.history.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;

@Data
@RequiredArgsConstructor
public class ApplicationStatusResponse {

    private final LocalDateTime dateTime;
    private final Status status;
}
