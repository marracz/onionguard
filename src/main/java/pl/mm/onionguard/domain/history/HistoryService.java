package pl.mm.onionguard.domain.history;

import pl.mm.onionguard.domain.history.model.ApplicationStatusResponse;
import pl.mm.onionguard.domain.history.model.ApplicationStatusSaveCommand;
import pl.mm.onionguard.domain.history.port.HistoryRegistry;

import java.util.List;

class HistoryService implements History {

    private final HistoryRegistry historyRegistry;

    public HistoryService(HistoryRegistry historyRegistry) {
        this.historyRegistry = historyRegistry;
    }

    @Override
    public List<ApplicationStatusResponse> getHistory(String applicationName) {
        return historyRegistry.getHistory(applicationName);
    }

    @Override
    public ApplicationStatusResponse save(ApplicationStatusSaveCommand command) {
        return historyRegistry.save(command);
    }
}
