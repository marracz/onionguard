package pl.mm.onionguard.domain.monitoring;

import pl.mm.onionguard.domain.monitoring.model.ApplicationCheckCommand;
import pl.mm.onionguard.domain.monitoring.model.ApplicationStatus;

public interface Monitoring {

    ApplicationStatus checkStatus(ApplicationCheckCommand applicationCheckCommand);
}
