package pl.mm.onionguard.domain.monitoring.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ReplicaStatus {

    private final String replicaIp;
    private final Status status;
}
