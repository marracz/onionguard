package pl.mm.onionguard.domain.monitoring.port;

import pl.mm.onionguard.domain.monitoring.model.ApplicationStatus;

import java.time.LocalDateTime;

public interface HistorySaveService {

    void save(LocalDateTime dateTime, ApplicationStatus applicationStatus);
}
