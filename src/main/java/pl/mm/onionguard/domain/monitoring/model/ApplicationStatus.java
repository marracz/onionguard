package pl.mm.onionguard.domain.monitoring.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.Collection;

@Data
@RequiredArgsConstructor
public class ApplicationStatus {

    private final String applicationName;
    private final Status status;
    private final Collection<ReplicaStatus> replicaStatuses;
}
