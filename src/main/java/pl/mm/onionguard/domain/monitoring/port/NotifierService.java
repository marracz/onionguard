package pl.mm.onionguard.domain.monitoring.port;

import pl.mm.onionguard.domain.monitoring.model.ApplicationCheckCommand;
import pl.mm.onionguard.domain.monitoring.model.ReplicaStatus;

import java.util.Collection;

public interface NotifierService {

    void notifyFailure(ApplicationCheckCommand applicationCheckCommand, Collection<ReplicaStatus> replicaStatuses);
}
