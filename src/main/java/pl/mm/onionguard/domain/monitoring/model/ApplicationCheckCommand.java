package pl.mm.onionguard.domain.monitoring.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.Collection;

@RequiredArgsConstructor
@Data
public class ApplicationCheckCommand {

    private final String name;
    private final Collection<ReplicaCheckCommand> replicaCheckCommands;
}
