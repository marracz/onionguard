package pl.mm.onionguard.domain.monitoring.port;

import pl.mm.onionguard.domain.monitoring.model.ReplicaCheckCommand;
import pl.mm.onionguard.domain.monitoring.model.ReplicaStatus;

public interface ReplicaStatusChecker {

    ReplicaStatus check(ReplicaCheckCommand replicaCheckCommand);
}
