package pl.mm.onionguard.domain.monitoring.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ReplicaCheckCommand {

    private final String ip;
    private final String healthcheck;
}
