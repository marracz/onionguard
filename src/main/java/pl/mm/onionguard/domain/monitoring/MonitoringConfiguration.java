package pl.mm.onionguard.domain.monitoring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.mm.onionguard.domain.monitoring.port.HistorySaveService;
import pl.mm.onionguard.domain.monitoring.port.NotifierService;
import pl.mm.onionguard.domain.monitoring.port.ReplicaStatusChecker;

@Configuration
class MonitoringConfiguration {

    @Bean
    public Monitoring monitoring(ReplicaStatusChecker replicaStatusChecker,
                                 NotifierService notifierService,
                                 HistorySaveService historySaveService) {
        return new MonitoringService(replicaStatusChecker, notifierService, historySaveService);
    }
}
