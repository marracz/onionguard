package pl.mm.onionguard.domain.monitoring;

import pl.mm.onionguard.domain.monitoring.model.*;
import pl.mm.onionguard.domain.monitoring.port.HistorySaveService;
import pl.mm.onionguard.domain.monitoring.port.NotifierService;
import pl.mm.onionguard.domain.monitoring.port.ReplicaStatusChecker;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.stream.Collectors;

class MonitoringService implements Monitoring {

    private final ReplicaStatusChecker replicaStatusChecker;
    private final NotifierService notifierService;
    private final HistorySaveService historySaveService;

    public MonitoringService(ReplicaStatusChecker replicaStatusChecker, NotifierService notifierService, HistorySaveService historySaveService) {
        this.replicaStatusChecker = replicaStatusChecker;
        this.notifierService = notifierService;
        this.historySaveService = historySaveService;
    }

    public ApplicationStatus checkStatus(ApplicationCheckCommand applicationCheckCommand) {
        final Set<ReplicaStatus> replicasStatus = applicationCheckCommand.getReplicaCheckCommands().stream()
                .map(this::checkSingleReplicaStatus)
                .collect(Collectors.toSet());
        final ApplicationStatus applicationStatus = new ApplicationStatus(applicationCheckCommand.getName(), calculateAllReplicasGeneralStatus(replicasStatus), replicasStatus);

        if (applicationStatus.getStatus() == Status.FAIL) {
            notifierService.notifyFailure(applicationCheckCommand, replicasStatus);
        }
        historySaveService.save(LocalDateTime.now(), applicationStatus);
        return applicationStatus;
    }

    private Status calculateAllReplicasGeneralStatus(Set<ReplicaStatus> replicaStatuses) {
        return replicaStatuses.stream()
                .allMatch(replicaStatus -> Status.OK == replicaStatus.getStatus()) ? Status.OK : Status.FAIL;
    }

    private ReplicaStatus checkSingleReplicaStatus(ReplicaCheckCommand replicaCheckCommand) {
        return replicaStatusChecker.check(replicaCheckCommand);
    }
}
