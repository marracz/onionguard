package pl.mm.onionguard.domain.notification;

import pl.mm.onionguard.domain.notification.port.NotificationChannel;

import java.util.List;

class NotificationService implements Notifier {

    private final List<NotificationChannel> notificationChannelList;

    public NotificationService(List<NotificationChannel> notificationChannelList) {
        this.notificationChannelList = notificationChannelList;
    }

    @Override
    public void sendNotification(String msg) {
        notificationChannelList.forEach(notificationChannel ->
                notificationChannel.sendNotification(msg));
    }
}
