package pl.mm.onionguard.domain.notification.port;

public interface NotificationChannel {

    void sendNotification(String msg);
}
