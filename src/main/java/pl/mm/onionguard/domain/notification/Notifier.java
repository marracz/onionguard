package pl.mm.onionguard.domain.notification;

public interface Notifier {

    void sendNotification(String msg);
}
