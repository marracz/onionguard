package pl.mm.onionguard.domain.notification;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.mm.onionguard.domain.notification.port.NotificationChannel;

import java.util.List;

@Configuration
class NotificationConfiguration {

    @Bean
    Notifier notificationService(List<NotificationChannel> notificationChannelList) {
        return new NotificationService(notificationChannelList);
    }

}
