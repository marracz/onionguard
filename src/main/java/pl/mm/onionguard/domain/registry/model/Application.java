package pl.mm.onionguard.domain.registry.model;

import lombok.Builder;
import lombok.Data;
import java.util.Collection;

@Data
@Builder
public class Application {

    private String name;

    private Collection<Replica> replicas;
}
