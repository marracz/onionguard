package pl.mm.onionguard.domain.registry.model;

public class ApplicationFetchFailedException extends RuntimeException {

    public ApplicationFetchFailedException(String message, Throwable cause) {
        super(message, cause);
    }
}
