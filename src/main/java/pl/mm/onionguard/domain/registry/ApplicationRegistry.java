package pl.mm.onionguard.domain.registry;

import pl.mm.onionguard.domain.registry.model.Application;

import java.util.Collection;

public interface ApplicationRegistry {

    Collection<Application> findAll();

}
