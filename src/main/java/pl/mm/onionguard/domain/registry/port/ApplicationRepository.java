package pl.mm.onionguard.domain.registry.port;

import pl.mm.onionguard.domain.registry.model.Application;
import pl.mm.onionguard.domain.registry.model.ApplicationFetchFailedException;

import java.util.Collection;

public interface ApplicationRepository {

    Collection<Application> findAll() throws ApplicationFetchFailedException;
}
