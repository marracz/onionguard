package pl.mm.onionguard.domain.registry;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.mm.onionguard.domain.registry.port.ApplicationRepository;

@Configuration
class ApplicationRegistryConfiguration {

    @Bean
    public ApplicationRegistry applicationRegistryService(ApplicationRepository applicationRepository) {
        return new ApplicationRegistryService(applicationRepository);
    }
}
