package pl.mm.onionguard.domain.registry;

import lombok.extern.slf4j.Slf4j;
import pl.mm.onionguard.domain.registry.model.Application;
import pl.mm.onionguard.domain.registry.model.ApplicationFetchFailedException;
import pl.mm.onionguard.domain.registry.port.ApplicationRepository;

import java.util.Collection;
import java.util.Collections;

@Slf4j
class ApplicationRegistryService implements ApplicationRegistry {

    private final ApplicationRepository applicationRepository;

    public ApplicationRegistryService(ApplicationRepository applicationRepository) {
        this.applicationRepository = applicationRepository;
    }

    public Collection<Application> findAll() {
        try {
            return this.applicationRepository.findAll();
        } catch (ApplicationFetchFailedException ex) {
            log.error("Cannot fetch applications, {}, {}", ex.getMessage(), ex);
            return Collections.EMPTY_LIST;
        }
    }
}
