package pl.mm.onionguard.domain.registry.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Replica {

    private String ip;
    private String healthcheck;
}
